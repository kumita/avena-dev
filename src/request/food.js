
/**
 * Created by Alma Patiño on 2018-12-01
 * 
 */

const username = "test@test.test";
const password = "test";
const api = "https://avena-dev.appspot.com/_ah/api/";
const token = "CkIKGgoEbmFtZRISGhBhY2VpdGUgZGUgY2Fub2xhEiBqC3N-YXZlbmEtZGV2chELEgRGb29kGICAgIC838gJDBgAIAA";

const getFood = ({ limit = 12, search = "" }) => {
    const headers = new Headers();
    headers.set('Authorization', `Basic ${Buffer.from(`${username}:${password}`).toString('base64')}`);

    const url = `${api}food/v1/food/smae?limit=${limit}&search=${search}&nextPageToken=${token}`,
        init = {
            method: 'GET',
            headers,
            mode: 'cors',
            cache: 'default'
        };

    return fetch(new Request(url, init))
        .then(response => response.json())
        .then(data => (data.items))
        .catch(error => console.error('Error:', error));
};

export { getFood };