/**
 * Created by Alma Patiño on 2018-12-01
 * 
 */

import React, { Component } from 'react';

class QuickSearch extends Component {
    state = {
        value: ""
    };

    handleChange = (event) => {
        this.setState({ value: event.target.value });
    }

    render() {
        const { onClick } = this.props;
        return (
            <div className="quick-search">
                <input type="search" id="search" placeholder="Buscar" value={this.state.value} onChange={this.handleChange} />
                <button className="icon" onClick={() => { onClick(this.state.value); }}><i className="fa fa-search"></i></button>
            </div>
        );
    }
}

export default QuickSearch;
