/**
 * Created by Alma Patiño on 2018-12-01
 * 
 */

import React, { Component } from 'react';

class Pagination extends Component {
    state = {
        maxPagination: 4,
        selectedItem: 0
    };

    handleSelectItem = (item) => {
        const { selectedItem } = this.state,
            { onClick } = this.props;

        if (item !== selectedItem) {
            this.setState({ selectedItem: item }, onClick(item));
        }
    }

    handleCleanState = () => {
        this.setState({ selectedItem: 0, maxPagination: 4 });
    }

    paginationItem = () => {
        const { selectedItem, maxPagination } = this.state;
        let itemArray = [];
        for (let i = maxPagination - 4; i <= maxPagination; i++) {
            itemArray = [...itemArray,
            <button
                key={i}
                onClick={() => { this.handleSelectItem(i); }}
                className={`btn ${selectedItem === i ? "is-active" : ""}`}>
                <li>{i + 1}</li>
            </button>
            ];
        }
        return itemArray.map(item => item);
    }

    handlePrevious = () => {
        const { selectedItem, maxPagination } = this.state,
            { onClick } = this.props;

        if (selectedItem - 1 >= 0) {
            if (selectedItem - 1 < maxPagination - 4) {
                return this.setState({ selectedItem: selectedItem - 1, maxPagination: maxPagination - 1 }, onClick(selectedItem - 1));
            }
            return this.setState({ selectedItem: selectedItem - 1 }, onClick(selectedItem - 1));
        }
        return;
    }

    handleNext = () => {
        const { selectedItem, maxPagination } = this.state,
            { onClick } = this.props;
        if (selectedItem + 1 > maxPagination) {
            return this.setState({ selectedItem: selectedItem + 1, maxPagination: maxPagination + 1 }, onClick(selectedItem + 1));
        }
        return this.setState({ selectedItem: selectedItem + 1 }, onClick(selectedItem + 1));
    }

    render() {
        const { enableNext } = this.props,
            { selectedItem } = this.state;
        return (
            <div className="container-pagination">
                <div className="pagination">
                    <ul>
                        <button disabled={selectedItem === 0} className="btn" onClick={this.handlePrevious}><li>{"<"}</li></button>
                        {this.paginationItem()}
                        <button disabled={!enableNext} className="btn" onClick={this.handleNext}><li>{">"}</li></button>
                    </ul>
                </div>
            </div>
        );
    }
}

export default Pagination;
