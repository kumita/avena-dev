/**
 * Created by Alma Patiño on 2018-12-01
 * 
 */

import React, { Component } from 'react';

class Card extends Component {
    render() {
        const { onClick, item } = this.props;
        return (
            <div className="card" onClick={onClick}>
                <div className="container-card">
                    <div className="title-card"><h4><b>{item.name}</b></h4></div>
                    <div className="container-list-card">
                        <ul className="list-card">
                            <li>{`Tipo: ${item.type}`}</li>
                            <li>{`Calorías: ${item.energyCal}`}</li>
                            <li>{`Unidad: ${item.unity}`}</li>
                            <li>{`Colesterol: ${item.cholesterol}`}</li>
                            <li>{`Carbohidratos: ${item.carbs}`}</li>
                            <li>{`Ácido graso saturado: ${item.saturatedFattyAcid}`}</li>
                            <li>{`Proteína: ${item.protein}`}</li>
                            <li>{`Lípidos: ${item.lipids}`}</li>
                        </ul>
                    </div>
                </div>
            </div>
        );
    }
}

export default Card;
