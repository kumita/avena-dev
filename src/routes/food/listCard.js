/**
 * Created by Alma Patiño on 2018-12-01
 * 
 */

import React from 'react';
import { Card } from '../../components';

const ListCard = ({ food, onClick }) => {
    return  food.map(f => <Card key={f.websafeKey} item={f} onClick={onClick} />);
};

export default ListCard;