/**
 * Created by Alma Patiño on 2018-12-01
 * 
 */

import React, { Component } from 'react';
import { getFood } from '../../request/food';
import { QuickSearch, Pagination } from '../../components';
import ListCard from './listCard';

class Food extends Component {
  state = {
    food: [],
    valueToSearch: "",
    enableNext: true
  };

  constructor(props) {
    super(props);
    this.pagination = React.createRef();
  }

  componentDidMount() {
    getFood({})
      .then((data) => this.setState({ food: data }));
  }

  handleSearch = (value) => {
    const { valueToSearch } = this.state;
    if (value.toUpperCase() !== valueToSearch.toUpperCase()) {
      getFood({ search: value })
        .then((data) => {
          const food = data || [];
          return this.setState({ food, valueToSearch: value, enableNext: !food.length === 0 },
            this.pagination.current.handleCleanState());
        });
    }
  }

  handlePagination = (value) => {
    const limit = ((value + 1) * 12) + 1,
      { valueToSearch } = this.state;

    getFood({ limit, search: valueToSearch })
      .then((data) => {
        if (data) {
          const size = data.length - 1,
            start = size - 12,
            newFood = data.slice(start, size);
          return this.setState({ food: newFood, enableNext: limit <= data.length });
        }
        return this.setState({ food: [] });
      });
  }

  handleGoToCheckout = () => {
    this.props.history.push('/checkout');
  }

  render() {
    const { food, enableNext } = this.state;
    return (
      <div>
        <div className="layout">
          <div className="content">
            <div style={{ background: '#fff', padding: 24, minHeight: 800 }}>
              <QuickSearch onClick={this.handleSearch} />
              <hr className="style-hr"></hr>
              <div>
                <section className="section-container">
                  <ListCard food={food} onClick={this.handleGoToCheckout}/>
                </section>
              </div>
              <Pagination ref={this.pagination} onClick={(key) => this.handlePagination(key)} enableNext={enableNext} />
            </div>
          </div>
          <div className="footer">
            Created by Alma Patiño
          </div>
        </div>
      </div>
    );
  }
}

export default Food;
