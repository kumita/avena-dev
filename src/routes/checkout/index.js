import React, { Component } from 'react';

class Checkout extends Component {
    state = {
        email: "",
        cardNumber: "",
        securityCode: "",
        cardExpirationMonth: "",
        cardExpirationYear: "",
        cardholderName: ""
    }

    handleChange = (value, key) => {
        this.setState({ [key]: value });
    }

    handleSubmit = (event) => {
        this.props.history.push('/food');
        event.preventDefault();
    }

    render() {
        return (
            <div className="checkout" onSubmit={this.handleSubmit}>
                <form>
                    <label>Total: $5.00</label>
                    <br></br>
                    <br></br>
                    <br></br>
                    <label>Email</label>
                    <input type="text" value={this.state.value} onChange={(event) => { this.handleChange(event.target.value, "email"); }}>
                    </input>
                    <label>Numero de tarjeta</label>
                    <input type="text" value={this.state.value} onChange={(event) => { this.handleChange(event.target.value, "cardNumber"); }}>
                    </input>
                    <label>Codigo de seguridad</label>
                    <input type="text" value={this.state.value} onChange={(event) => { this.handleChange(event.target.value, "securityCode"); }}>
                    </input>
                    <label>Mes de expiración</label>
                    <input type="text" value={this.state.value} onChange={(event) => { this.handleChange(event.target.value, "cardExpirationMonth"); }}>
                    </input>
                    <label>Año de expiración:</label>
                    <input type="text" pattern="\d*" value={this.state.value} onChange={(event) => { this.handleChange(event.target.value, "cardExpirationYear"); }}>
                    </input>
                    <label>Nombre del propietario de la tarjeta</label>
                    <input type="text" value={this.state.value} onChange={(event) => { this.handleChange(event.target.value, "cardholderName"); }}>
                    </input>
                    <input type="submit" value="Pagar">
                    </input>
                </form>
            </div>
        );
    }
}

export default Checkout;
