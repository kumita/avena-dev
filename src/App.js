import React, { Component } from 'react';
import {
  BrowserRouter as Router,
  Route,
  Redirect
} from 'react-router-dom';
import { Food, Checkout } from './routes';

const routes = [
  {
    path: '/food',
    component: Food
  },
  {
    path: '/checkout',
    component: Checkout
  }
]

class App extends Component {
  render() {
    return (
      <Router>
        <Redirect from="/" to="/food" />
        {routes.map((route) => (
          <Route
            key={route.path}
            path={route.path}
            component={route.component}
          />
        ))}
      </Router>
    );
  }
}

export default App;
